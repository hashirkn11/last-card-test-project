$(window).scroll(function () {
    var scroll_position = $(window).scrollTop();
    if (scroll_position > 20) {
      $('nav').addClass('scroll_sticky')
    } else {
      $('nav').removeClass('scroll_sticky')
    }
  })

  // script for swiper js
  var swiper = new Swiper(".mySwiper", {
    navigation: {
      nextEl: ".button-next",
      prevEl: ".button-prev",
    },
  });